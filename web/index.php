<!DOCTYPE html>
<html>
  <head>
    <title>Don't Panic</title>
  </head>
  <body>

    <?php
      $servername = "mysql";
      $username = "root";
      $password = "password";
      $dbname = "test";

      $conn = new mysqli($servername, $username, $password, $dbname);

      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT message FROM messages";
      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          echo "<h1>" . $row["message"] . "</h1>";
        }
      } else {
        echo "0 results";
      }
      $conn->close();
    ?> 

  </body>
</html> 